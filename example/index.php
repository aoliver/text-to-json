<?php
    include '../txttojson.php';
    $ttj = new texttojson;

    function format_the_number($atts) {
        if($atts["type"] === "float"){
            return number_format($atts["content"],2);
        }
    };

    $ttj->map_method(["format_the_number"])->process(file_get_contents('thetext.txt'))->render();
?>