<?php
/*
    Text to JSON
    @license: MIT - http://opensource.org/licenses/MIT
    @version: 1.2.0
    @author: Alex Oliver
    @Repo: https://bitbucket.org/aoliver/text-to-json

    Copyright (c) 2016 Alex Oliver
*/
    class texttojson{
        var $buildArray = [];
        var $processdArray = [];
        var $methods = [];

        function __construct($inputString = false){
            if($inputString){
                $this->process($inputString);
            }   
        }

        public function process($inputString){

            $sectionID = '';
            $this->buildArray = [];
            $this->processdArray = [];

            //remove comment blocks
            $inputString = preg_replace('/\/\*(?!(\<\!\[CDATA\[)|(\]\]>))(.*?)\*\//', '', $inputString);

            //cycle through each line
            foreach(explode("\n", $inputString) as $thisLine){
                
                //new index found
                if(preg_match('/^\[.*?]/', $thisLine, $matches) && count($matches) === 1){
                    $theOptions = $this->returnOptions($matches[0]);
                    $sectionID = $theOptions[0];
                    
                    $thisLine = preg_replace('/^\[.*?]/', '', $thisLine);                    
                    $this->buildArray[$sectionID] = $thisLine;

                    $theAttrs = [
                        "content" => $thisLine
                    ];

                    if(count($theOptions) >= 2){
                        //remove first entry in array
                        array_shift($theOptions);
                        
                        foreach($theOptions as $thisOption){
                            $theAttrs[$thisOption[0]] = $thisOption[1];
                        }
                        $this->buildArray[$sectionID] = $theAttrs;
                    }
                    
                }

                //add to this index and linebreak
                elseif($sectionID !== ''){
                    if(is_array($this->buildArray[$sectionID])){
                        $this->buildArray[$sectionID]['content'] .= $thisLine."{*%nl%*}";
                    } else {
                        $this->buildArray[$sectionID] .= $thisLine."{*%nl%*}";
                    }
                    
                }
            }

            //add to process array
            foreach($this->buildArray as $key=>$value){

                //process linebreaks
                $value = $this->process_linebreaks($value);

                if(is_array($value) && isset($value['content'])){
                    $value['content'] = $this->process_linebreaks($value['content']);
                }

                //process mapped function
                if(is_array($value) && isset($value["mapped"]) && $mapped = $this->process_mapped_type("mapped", $value['mapped'], $value)){
                    $value["content"] = $mapped;
                }

                //add to processed array
                $this->addToProcessedArr($key, $value);
            }

            return $this;
        }

        private function process_linebreaks($value = ""){
            if(gettype($value) === 'string'){
                //remove end new lines or {*%nl%*}
                $value = preg_replace('/({\*%nl%\*})+$/', '', $value);

                //replace {*%nl%*} with new lines
                $value = preg_replace('/({\*%nl%\*})/', "\n\n", $value);

            }
            return $value;
        }

        //check for options
        private function returnOptions($strParams){
            $options = explode(' ', str_replace([']','['], '', $strParams));
            $returnOptions = [array_shift($options)];
            foreach($options as $thisOption){
                if(count($varVal = explode("=", $thisOption)) === 2){
                    $returnOptions[] = $varVal;
                }
            }
            return $returnOptions;
        }

        //add to working array
        private function addToProcessedArr($sectionID = "", $thisLine = ""){
            if(count($subArr = explode('->', $sectionID)) > 1){
                
                if(is_array($thisLine) || trim($thisLine) !== ''){
                    $nestedSet = array();
                    
                    foreach (array_reverse($subArr) as $arr){
                        $nestedSet = array($arr => $nestedSet);

                        if(end($subArr) === $arr){
                            $nestedSet[$arr] = $thisLine;
                        }

                    }
                    $this->processdArray = array_replace_recursive($this->processdArray, $nestedSet);
                }

            } else {
                if (!isset($this->processdArray[$sectionID])) {
                    $this->processdArray[$sectionID] = '';
                }
                $this->processdArray[$sectionID] = $thisLine;
            }
        }

        //echo JSON formatted string
        public function render(){
            echo json_encode($this->processdArray, JSON_PRETTY_PRINT);
        }

        //return values in a php array
        public function returnArray(){
            return $this->processdArray;
        }

        //map to function
        public function map_method($the_methods = []){
            if(is_array($the_methods)){
                foreach($the_methods as $theMethod){
                    if(gettype($theMethod) === 'string'){
                        $this->methods[$theMethod] = true;
                    }
                }
            }
            return $this;
        }

        //process mapped functions
        private function process_mapped_type($type = "", $input = "", $attrs = []){
            if($type === "mapped"){
                if(isset($this->methods[$input]) && is_callable($input)){
                    return call_user_func_array($input, [$attrs]);
                }
                return $attrs["content"];
            }
            return $attrs["content"];
        }
    }
?>