#Text to JSON

Text to JSON is a small PHP class that will convert a standard text document into a JSON string.

##How does it work

In order for this little library to do its magic there are a few rules that need to be abided by.

The below example is a sample of a text file that could be passed.

```text
[title]This is the page title


[p1]
Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec in lacus mauris.
Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.
```

JSON is a key based data format so there needs to be a key bound into the text document. This is done by starting each section with [], this lets the library know when to assign a new key to the following content.

In the above example you can see [title] and [p1] keys have been specified. The exported JSON string will look like the following:

```json
{
    "title": "This is the page title",
    "p1": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec in lacus mauris."
}
```

##Great! Now how do I use it?

Installation and usage is pretty straight forward.

```php
<?php
    include 'txttojson.php';
    $ttj = new texttojson(file_get_contents('thetext.txt'));
    $ttj->render();
?>
```

In the above example the class is first included and then a new instance is created ($ttj) passing our text string.

The render() method will echo out a JSON formatted string.

##Nested keys

Nesting key values can also be achieved.

```text
[each->products->product1->title]This is the title
[each->products->product1->desc]this is a description
[each->products->product1->price]2.99
```

The above will be printed like so.

```json
{
    "each": {
        "products": {
            "product1": {
                "title": "This is the title",
                "desc": "this is a description",
                "price": "2.99"
            }
        }
    }
}
```

##Adding Parameters to keys

Parameters can be added to keys. To keep things simple spaces in each keys value is not allowed.

```text
[each->products->product1->title type=string]This is the title
[each->products->product1->desc type=textfield]this is a description
[each->products->product1->price type=float currency=pound]2.99
```

The above will be printed like so.

```json
{
    "each": {
        "products": {
            "product1": {
                "title": "This is the title",
                "title-type": "string",
                "desc": "this is a description",
                "desc-type": "textfield",
                "price": "2.99",
                "price-type": "float",
                "price-currency": "pound"
            }
        }
    }
}
```

##Mapping functions on key parameter types

Functions can now be mapped to nodes using the map_method method.

```text
[each->products->p01->title]This is the title
[each->products->p01->desc]this is a description
[each->products->p01->price type=float currency=pound mapped=format_the_number]4.99 ea

[each->products->p02->title]This is another title
[each->products->p02->desc]this is a description
[each->products->p02->price type=float currency=pound mapped=format_the_number]2.99ea
```
Linking a method to a node can be acheivied by adding the 'mapped' pramater followed by the method name for example 'mapped=format_the_number'.

```php
<?php
    include '../txttojson.php';
    $ttj = new texttojson;

    function format_the_number($atts) {
        if($atts["type"] === "float"){
            return number_format($atts["content"],2);
        }
    };

    $ttj->map_method(["format_the_number"])->process(file_get_contents('thetext.txt'))->render();
?>
```

The method 'format_the_number' will return a proper formated number.